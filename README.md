# Sign-Language-Alphabets
Alphabets from various sign languages around the world in pictures.

Everything is licenced under:

[![Licence](https://gitlab.com/Unrepentant-Atheist/Sign-Language-Alphabets/raw/a201fd9ca6bb227ad3597f498a6ad5482148897e/CC_licence.png)](https://creativecommons.org/licenses/by-nc-sa/4.0/)